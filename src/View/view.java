package View;

import java.util.Scanner;

import Controller.controller;
import models.structures.data.ListaEncadenada;

public class view {

	private static void printMenu(){
		System.out.println("1. Para leer el archivo de peliculas del Taller 2");
		System.out.println("2. Para saber el numero de elementos");
		System.out.println("3. Para saber solo el nombre de las peliculas");
		System.out.println("4. Para obtener una lista de a�os");

	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printMenu();
		Scanner sc = new Scanner(System.in);
		int option = sc.nextInt();
		switch (option) {
		case 1:
			controller.LeerCSVPeliculas();
			break;
		case 2:
			int quantity = controller.CantidadDeElementos();
			System.out.println(""+quantity);
			break;
		case 3:
			controller.nombreDePeliculas();
			break;
		case 4:
			controller.aniosDeLasPeliculas();
			break;
		default:
			break;
		}
	}

}
