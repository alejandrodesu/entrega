package model.data_structures;

import java.util.ArrayList;

public class Stack<T> implements IStack<T> 
{
	ListaEncadenada lista;
	
	public Stack(ArrayList<Character> expresion)
	{
		lista = new ListaEncadenada<T>();
	}
	
	@Override
	public void push(T elem) 
	{
		lista.agregarElementoPrincipio(elem);
	}

	@Override
	public T pop() 
	{
		return (T) lista.quitarElementoPrincipio();
	}

	@Override
	public int size() 
	{
		return lista.size();
	}
	
	public T darElemento(int pos)
	{
		return (T) lista.darElemento(pos);
	}

	@Override
	public boolean isEmpty() 
	{
		if(lista.darNumeroElementos()==0)
		{
			return true;
		}
		else
			return false;
	}

}
