package models.structures.data;

public class Node<String> 
{
	private Node<String> next;  
	private String item; 
	
	public Node()
	{
		next=null;
		this.item=null;
	}
	
	public Node<String> getNext() {
		return next;
	}

	public String getItem() {
		return item;
	}
	
	public void setItem(Node<String> node) {
		this.item = (String) node;
	}
	
	public void setNext(Node<String> next) {
		this.next = next;
	}

	
}
