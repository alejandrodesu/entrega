package models.structures.data;

import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.omg.CosNaming.NamingContextPackage.NotEmpty;
import org.w3c.dom.ls.LSInput;

	
public class ListaEncadenada<String> implements ILista<String> {

	private Node<String> primero;
	private Node<String> actual;
	private int listSize;
	
	public ListaEncadenada()
	{
		primero = new Node<String>();
		actual = primero;
	}
	
	public int size()
	{
		return listSize;
	}
	
	@Override
	public Iterator<String> iterator() 
	{
		return new Iterator<String>() 
			{
			private Node<String> actualT=null;
	
			public boolean hasNext() 
			{
				if(actualT==null)return primero.getItem()!=null;
				else return actualT.getNext() != null;
			}
	
			public String next() 
			{
				if(actualT==null)
				{
					actualT=primero;
					if(actualT==null)return null;
					else return actualT.getItem();
				}
				else
				{
					actualT = actualT.getNext();
					return actualT.getItem();
				}
	
			}
		};
}

	@Override
	public void agregarElementoFinal(String elem) 
	{
		if(primero.getItem() == null)
		{
			primero.setItem((Node<String>) elem);
			return;
		}
		else
		{
			Node<String> act = primero;
			while(act != null)
			{
				if (act.getNext()== null)
				{
					act.setItem(new Node<String>());
					act.getNext().setItem((Node<String>) elem);
					actual = act.getNext();
					break;
				}
				act = act.getNext();
			}
		}
		listSize++;
	}

	@Override
	public String darElemento(int pos) 
	{
		actual = primero;
		int contador = 0;
		while(contador<pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return (String) actual.getItem();
	}

	@Override
	public int darNumeroElementos() 
	{
		int contador = 0;
		Node<String> act = primero;
		while(act != null)
		{
			contador ++;
			act = act.getNext();
		}
		return contador;
	}

	@Override
	public String darElementoPosicionActual() 
	{
		if(actual!=null)
		{
			return actual.getItem();
		}
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() 
	{
		if(actual != null && actual.getNext() != null)
		{
			actual = actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() 
	{
		Node<String> act = primero;
		while(act != null)
		{
			if (act.getNext() != null && act.getNext().equals(actual))
			{
				actual = act;
				return true;
			}
			act = act.getNext();
		}
		return false;
	}

}