package Model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import models.structures.data.ListaEncadenada;


public class modelo {

	private static final String RUTA = "./data/movies.csv";

	public void leerPeliculas(){
		try {
			//List<String> lines = Files.readAllLines(Paths.get(RUTA));
			//lines =(String)Files.readAllLines(Paths.get(RUTA));
			BufferedReader br = new BufferedReader(new FileReader(RUTA));
			String line = br.readLine();
			while (line!=null) {
				line= line.replace("\"", "");
				System.out.println(line);
				line = br.readLine();
			}
			/*
			for (String line : lines) {
				line = line.replace("\"", "");
				System.out.println(line);
			}
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int cantidadDeElementos()
	{
		int contador =0;
		try {
			/*List<String> lines = Files.readAllLines(Paths.get(RUTA));
			for (String line : lines) {
				if (line.isEmpty()==false) {
					contador++;
				}
			}*/
			BufferedReader br = new BufferedReader(new FileReader(RUTA));
			String line = br.readLine();
			while (line!=null) {
				if (line.isEmpty()==false) {
					contador++;
				}	
				line = br.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contador;
	}

	public void nombreDePeliculas ()
	{
		try {
			/*List<String> lines = Files.readAllLines(Paths.get(RUTA));
			for (String line : lines) {
				line = line.replace("\"", "");
				String nombre[] = line.split(",");
				System.out.println(nombre[1]);
			}*/
			BufferedReader br = new BufferedReader(new FileReader(RUTA));
			String line = br.readLine();
			while (line!=null) {
				line= line.replace("\"", "");
				String nombre[] = line.split(",");
				System.out.println(nombre[1]);
				line = br.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void aniosDeLasPeliculas() {
		try {
			BufferedReader br = new BufferedReader(new FileReader(RUTA));
			String line = br.readLine();
			while (line!=null) {
				line= line.replace("(", ",");
				line= line.replace(")", "");
				String anios [] = line.split(",");
				if (anios[2].startsWith("1")||anios[2].startsWith("2")
						&&anios[2].endsWith("1")||anios[2].endsWith("2")||anios[2].endsWith("3")
						||anios[2].endsWith("4")||anios[2].endsWith("5")||anios[2].endsWith("6")
						||anios[2].endsWith("7")||anios[2].endsWith("8")||anios[2].endsWith("9")
						||anios[2].endsWith("0")) {
					System.out.println(anios[2]);
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
